<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Design extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// $data['page'] = 'v_design';
		// $this->load->view('extra/template', $data);

		$this->load->view('v_editor');

	}

}

/* End of file Design.php */
/* Location: ./application/controllers/Design.php */