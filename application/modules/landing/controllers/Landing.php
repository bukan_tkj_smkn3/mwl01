<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page'] = 'v_landing';
		$this->load->view('extra/template', $data);
	}

}

/* End of file Landing.php */
/* Location: ./application/controllers/Landing.php */