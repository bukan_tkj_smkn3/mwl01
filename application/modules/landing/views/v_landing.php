<!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#main-slide" data-slide-to="0" class="active"></li>
          <li data-target="#main-slide" data-slide-to="1"></li>
          <li data-target="#main-slide" data-slide-to="2"></li>
          <li data-target="#main-slide" data-slide-to="3"></li>
        </ol>
        <!--/ Indicators end-->

        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="<?php echo base_url('assets/theme/') ?>images/slider/banner01.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span>Feature  <strong>Wall</strong></span>
                              </h2>
                <p class="animated3"><a href="#" class="slider btn btn-system btn-large">Customize Now</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="<?php echo base_url('assets/theme/') ?>images/slider/banner02.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated6 white">
                                <span><strong>SCREEN</strong></span>
                            </h2>
                <p class="animated5"><a href="#" class="slider btn btn-system btn-large">Customize Now</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="<?php echo base_url('assets/theme/') ?>images/slider/banner05.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated6 white">
                                <span><strong>FACADE</strong></span>
                            </h2>
                <div class="">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="#">Customize Now</a>
                </div>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="<?php echo base_url('assets/theme/') ?>images/slider/banner04.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated6 white">
                                <span><strong>CEILING</strong></span>
                            </h2>
                <div class="">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="#">Customize Now</a>
                </div>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->

        <!-- Controls -->
        <a class="left carousel-control" href="#main-slide" data-slide="prev">
          <span><i class="fa fa-angle-left"></i></span>
        </a>
        <a class="right carousel-control" href="#main-slide" data-slide="next">
          <span><i class="fa fa-angle-right"></i></span>
        </a>
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->
    <div class="hr5" style="margin-top:45px; margin-bottom:35px;"></div>
    <!-- Start Services Section -->
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
            <h1>The <strong>Steps</strong></h1>
          </div>
          <!-- Start Service Icon 1 -->
          <div class="col-md-12">
            <video class="col-md-12 col-sm-12" autoplay loop>
              <source src="<?php echo base_url('assets/theme/') ?>vidio/the_step.mp4" type="video/mp4" >
            </video>
          </div>

          <!-- Start Service Icon 5 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="05">
            <div class="service-icon">
              
            </div>
            <div class="service-content">
              <h4>Design</h4>
              <p>You upload an image, select all the parameters, and see the price instantly.</p>
            </div>
          </div>
          <!-- End Service Icon 5 -->

          <!-- Start Service Icon 6 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="06">
            <div class="service-icon">

            </div>
            <div class="service-content">
              <h4>Develop</h4>
              <p>We help you resolve the details, from door cutouts to earthquake resistance.</p>
            </div>
          </div>
          <!-- End Service Icon 6 -->

          <!-- Start Service Icon 7 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="07">
            <div class="service-icon">
              
            </div>
            <div class="service-content">
              <h4>Build</h4>
              <p>We build your custom ImageWall system to your specifications.</p>
            </div>
          </div>
          <!-- End Service Icon 7 -->

          <!-- Start Service Icon 8 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="08">
            <div class="service-icon">
              
            </div>
            <div class="service-content">
              <h4>Ship</h4>
              <p>We deliver your system, carefully packed and ready for easy assembly.</p>
            </div>
          </div>
          <!-- End Service Icon 8 -->
          <!-- End Service Icon 1 -->
        </div>
      </div>
      <!-- .container -->
      <div class="hr5" style="margin-top:45px; margin-bottom:35px;"></div>
      <div class="container">

        <!-- Start Big Heading -->
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
          <h1>Our Great <strong>Material</strong></h1>
        </div>
        <!-- End Big Heading -->

        <!-- Some Text -->
        <p class="text-center">We believe in real metals. When well cared for, textured finishes and chemical patinas do not wear away or deteriorate. 
          They expose and interact with the inherent color and luster of the base metal, creating surfaces with character.</p>
        <!-- Start Team Members -->
        <div class="row">
          <!-- Start Memebr 1 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="03">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="<?php echo base_url('assets/theme/') ?>images/materials/steel.jpg" />
                <div class="member-name">STEEL 
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Steel is strong, heavy, and distinctive.</p>
              </div>
            </div>
          </div>
          <!-- End Memebr 1 -->


          <!-- Start Memebr 2 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="04">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="<?php echo base_url('assets/theme/') ?>images/materials/stainless_steel.jpg" />
                <div class="member-name">STAINLESS STEEL 
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Stainless Steel stays shiny as it ages, no coatings needed.</p>
              </div>
            </div>
          </div>
          <!-- End Memebr 2 -->

          <!-- Start Memebr 3 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="05">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="<?php echo base_url('assets/theme/') ?>images/materials/copper.jpg" />
                <div class="member-name">COPPER
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Copper is full of vibrant color which intensifies with patination.</p>
              </div>
            </div>
          </div>
          <!-- End Memebr 3 -->

          <!-- Start Memebr 4 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="06">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="<?php echo base_url('assets/theme/') ?>images/materials/alumunium.jpg" />
                <div class="member-name">ALUMINUM
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Aluminum is best for when exact color matters.</p>
              </div>
            </div>
          </div>
          <!-- End Memebr 4 -->

        </div>
        <!-- End Team Members -->

      </div>
      <!-- .container -->

      <div class="hr5" style="margin-top:45px; margin-bottom:35px;"></div>
      <div class="container">

        <!-- Start Big Heading -->
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
          <h1><strong>Gallery</strong></h1>
        </div>
        <!-- End Big Heading -->

        <!--Start Recent Projects-->
          <div class="recent-projects">
            <h4 class="title"><span>Gallery</span></h4>
            <div class="projects-carousel touch-carousel">

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a class="lightbox" data-lightbox-type="ajax" href="https://vimeo.com/78468485">
                      <div class="thumb-overlay"><i class="fa fa-play"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner01.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a class="lightbox" title="This is an image title" href="images/portfolio-big-01.jpg">
                      <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner02.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <div class="portfolio-thumb">
                    <a href="#">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner03.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a href="#">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner04.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a class="lightbox" title="This is an image title" href="images/portfolio-big-02.jpg">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner05.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                   <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a href="#">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner02.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a class="lightbox" title="This is an image title" href="images/portfolio-big-03.jpg">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner03.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                    <a href="#">
                      <div class="thumb-overlay"><i class="fa fa-link"></i></div>
                      <img alt="" src="<?php echo base_url('assets/theme/') ?>images/slider/banner04.jpg" />
                    </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4>Sample Customer</h4>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->

            </div>
          </div>
          <!--End Recent Projects-->

      </div>
      <!-- .container -->
    </div>
    <!-- End Team Member Section -->
    <div class="hr5" style="margin-top:45px; margin-bottom:35px;"></div>
    <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
      <h1><strong>Contact</strong></h1>
    </div>

    <!-- Start Map -33.881680, 151.188280 -->
    <div id="map" data-position-latitude="-33.881680" data-position-longitude="151.188280"></div>
    <script>
      (function($) {
        $.fn.CustomMap = function(options) {

          var posLatitude = $('#map').data('position-latitude'),
            posLongitude = $('#map').data('position-longitude');

          var settings = $.extend({
            home: {
              latitude: posLatitude,
              longitude: posLongitude
            },
            text: '<div class="map-popup"><h4>Web Development | ZoOm-Arts</h4><p>A web development blog for all your HTML5 and WordPress needs.</p></div>',
            icon_url: $('#map').data('marker-img'),
            zoom: 15
          }, options);

          var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);

          return this.each(function() {
            var element = $(this);

            var options = {
              zoom: settings.zoom,
              center: coords,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false,
              scaleControl: false,
              streetViewControl: false,
              panControl: true,
              disableDefaultUI: true,
              zoomControlOptions: {
                style: google.maps.ZoomControlStyle.DEFAULT
              },
              overviewMapControl: true,
            };

            var map = new google.maps.Map(element[0], options);

            var icon = {
              url: settings.icon_url,
              origin: new google.maps.Point(0, 0)
            };

            var marker = new google.maps.Marker({
              position: coords,
              map: map,
              icon: icon,
              draggable: false
            });

            var info = new google.maps.InfoWindow({
              content: settings.text
            });

            google.maps.event.addListener(marker, 'click', function() {
              info.open(map, marker);
            });

            var styles = [{
              "featureType": "landscape",
              "stylers": [{
                "saturation": -100
              }, {
                "lightness": 65
              }, {
                "visibility": "on"
              }]
            }, {
              "featureType": "poi",
              "stylers": [{
                "saturation": -100
              }, {
                "lightness": 51
              }, {
                "visibility": "simplified"
              }]
            }, {
              "featureType": "road.highway",
              "stylers": [{
                "saturation": -100
              }, {
                "visibility": "simplified"
              }]
            }, {
              "featureType": "road.arterial",
              "stylers": [{
                "saturation": -100
              }, {
                "lightness": 30
              }, {
                "visibility": "on"
              }]
            }, {
              "featureType": "road.local",
              "stylers": [{
                "saturation": -100
              }, {
                "lightness": 40
              }, {
                "visibility": "on"
              }]
            }, {
              "featureType": "transit",
              "stylers": [{
                "saturation": -100
              }, {
                "visibility": "simplified"
              }]
            }, {
              "featureType": "administrative.province",
              "stylers": [{
                "visibility": "on"
              }]
            }, {
              "featureType": "water",
              "elementType": "labels",
              "stylers": [{
                "visibility": "on"
              }, {
                "lightness": -25
              }, {
                "saturation": -100
              }]
            }, {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [{
                "hue": "#ffff00"
              }, {
                "lightness": -25
              }, {
                "saturation": -97
              }]
            }];

            map.setOptions({
              styles: styles
            });
          });

        };
      }(jQuery));

      jQuery(document).ready(function() {
        jQuery('#map').CustomMap();
      });
    </script>
    <!-- End Map -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-8">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Contact Us</span></h4>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" class="requiredField" placeholder="Subject" name="subject">
                </div>
              </div>

              <div class="form-group">

                <div class="controls">
                  <textarea rows="7" placeholder="Message" name="message"></textarea>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system btn-large">Send</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>

          <div class="col-md-4">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Information</span></h4>

            <!-- Some Info -->
            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.</p> -->

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:10px;"></div>

            <!-- Info - Icons List -->
            <ul class="icons-list">
              <li><i class="fa fa-globe">  </i> <strong>Address:</strong> 1234 Street Name, Bangladesh.</li>
              <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong> info@yourcompany.com</li>
              <li><i class="fa fa-mobile"></i> <strong>Phone:</strong> +12 345 678 001</li>
            </ul>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:15px;"></div>

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Working Hours</span></h4>

            <!-- Info - List -->
            <ul class="list-unstyled">
              <li><strong>Monday - Friday</strong> - 9am to 5pm</li>
              <li><strong>Saturday</strong> - 9am to 2pm</li>
              <li><strong>Sunday</strong> - Closed</li>
            </ul>

          </div>

        </div>

      </div>
    </div>
    <!-- End content -->


    <!-- Start Client/Partner Section -->
    <div class="partner">
      <div class="container">
        <div class="row">

          <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1>Our Happy <strong>Clients</strong></h1>
            <p class="title-desc">Partners We Work With</p>
          </div>
          <!-- End Big Heading -->

          <!--Start Clients Carousel-->
          <div class="our-clients">
            <div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="5" data-navigation="true">

              <?php for ($i=0; $i < 8; $i++) { ?>
                <!-- Client 1 -->
                <div class="client-item item">
                  <a href="#"><img src="<?php echo base_url('assets/theme/') ?>images/logo_sample.jpg" alt="" /></a>
                </div>
              <?php } ?>

            </div>
          </div>
          <!-- End Clients Carousel -->
        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Client/Partner Section -->